'use strict';
const program = require('../node_modules/commander');
const colors = require('../node_modules/colors');
const pkg = require('../package.json');
const cli = require('./cli');

const log = require('./Logger');

program
  .version(pkg.version)
  .option('-m, --message [message]', 'set message to be printed.')
  .on('—help', function() {
    log(' Examples:');
    log('');
    log(' $ cli --message hello');
  })
  .parse(process.argv);

if (process.argv.length === 2) {
  program.help();
} else {
  try {
    cli.print({
      message: program.message
    });
  } catch (_error) {
    log.Logger(`[ ${colors.white('cli')} ] ${colors.red(_error.toString())}`);
  }
}
