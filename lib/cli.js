'use strict';

const colors = require('../node_modules/colors');
const log = require('./Logger');

exports.print = (options) => {
  if (options && options.message && typeof options.message === 'string') {
    return log.Logger(`[ ${colors.white('cli')} ] ${colors.cyan(options.message.toString())}`);
  } else {
    throw new Error('no message defined to print!');
  }
};
