'use strict';

exports.Logger = (message) => {
  /*eslint-disable */
  //suppress all warnings between comments
  return console.log(message);
  /*eslint-enable */
};
